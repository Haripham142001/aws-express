var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var AWS = require("aws-sdk");
var multer = require("multer");
var multerS3 = require("multer-s3");
const fs = require("fs");

var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");

var app = express();

const accessKey = "AKIAYKG6VXXVRYPPSLC6";
const secretKey = "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";

AWS.config.update({
  secretAccessKey: secretKey,
  accessKeyId: accessKey,
  region: "us-west-1",
  Bucket: "test-upload-hari",
});

const s3 = new AWS.S3();
// var upload = multer({
//   storage: multerS3({
//     s3: s3,
//     acl: "public-read",
//     bucket: "test-upload-hari",
//     key: function (req, file, cb) {
//       console.log(file);
//       cb(null, file.originalname); //use Date.now() for unique file keys
//     },
//   }),
// });
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "uploads");
  },
  filename: function (req, file, cb) {
    const filename = Date.now() + "-" + Math.round(Math.random() * 1e9);
    cb(null, filename + "-" + file.originalname);
  },
});
const upload = multer({ storage: storage });

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);
app.use("/users", usersRouter);

app.post("/upload", upload.single("myFile"), function (req, res, next) {
  const file = req.file.filename;
  if (file) {
    s3.putObject({
      Bucket: "test-upload-hari",
      Body: fs.readFileSync("./uploads/" + file),
      Key: `file_${file}`,
    })
      .promise()
      .then((res) => {
        console.log(`Upload succeeded - `, res);
      })
      .catch((err) => {
        console.log("Upload failed:", err);
      });
    res.render('index');
  } else {
    res.json({ message: "tạch" });
  }
});

app.get("/upload", (req, res) => {
  // console.log(req);
  res.render("index", { title: "Hello" });
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  console.log(err);
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

app.listen(8080, () => {
  console.log("Server is running");
});

module.exports = app;
